# Exchange Rate Api

Exchange rate api is just an exercise.

It uses the [European Central Bank](https://www.ecb.europa.eu/stats/eurofxref/.xml) to retrieve the most recent list of
exchange rates to Euro.

## Endpoints

Historical euro exchange rate

- year exchange rates
- all exchange rates (paged)
- last 90 days exchange rates

Daily exchange rate

- daily exchange rate per currency
- daily exchange rate
- daily available exchange rate currencies

## Terminal

Can be called when running by using curl

```shell
$ curl -X 'GET' 'http://localhost:8080/daily/exchange-rate/USD'
```

## Database

Postgresql run via docker compose config found in `docker/docker-compose`.

```shell
$ docker-compose -f ./docker/docker-compose.yaml up
```

- More instructions on **makefile**.
