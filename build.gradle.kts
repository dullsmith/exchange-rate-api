plugins {
    id("org.springframework.boot") version "3.1.4"
    id("io.spring.dependency-management") version "1.1.3"
    id("org.jetbrains.kotlinx.kover") version "0.7.3"
    id("org.jetbrains.kotlin.jvm") version "1.9.10"
    id("org.jetbrains.kotlin.plugin.spring") version "1.9.10"
    id("org.flywaydb.flyway") version "9.8.1"
}

group = "io.exchangerate"
// x-release-please-start-version
version = "2.0.6"
// x-release-please-end
java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-cache")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.postgresql:postgresql:42.6.0")
    implementation("com.h2database:h2:2.2.224")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.15.2")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-xml:2.15.2")
    implementation("org.springdoc:springdoc-openapi-starter-webmvc-ui:2.2.0")
    implementation("com.squareup.retrofit2:converter-jackson:2.9.0")
    implementation("io.github.microutils:kotlin-logging-jvm:3.0.5")
    implementation("com.github.ben-manes.caffeine:caffeine:3.1.5")

    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(module = "mockito-core")
        exclude(module = "junit-vintage")
    }
    testImplementation("io.mockk:mockk:1.13.8")
    testImplementation("com.ninja-squad:springmockk:4.0.2")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")

        jvmTarget = "17"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()

    testLogging {
        events("passed", "skipped", "failed")
    }
}

tasks.withType<org.flywaydb.gradle.task.FlywayInfoTask> {
    url = "jdbc:postgresql://0.0.0.0:5432/rates"
    user = "ratesapi"
    password = "somepassword"
    locations = arrayOf("filesystem:src/main/resources/db/migration")
}

tasks.withType<org.flywaydb.gradle.task.FlywayCleanTask> {
    cleanDisabled = false
    url = "jdbc:postgresql://0.0.0.0:5432/rates"
    user = "ratesapi"
    password = "somepassword"
    locations = arrayOf("filesystem:src/main/resources/db/migration")
}

tasks.withType<org.flywaydb.gradle.task.FlywayMigrateTask> {
    url = "jdbc:postgresql://0.0.0.0:5432/rates"
    user = "ratesapi"
    password = "somepassword"
    baselineOnMigrate = true
    locations = arrayOf("filesystem:src/main/resources/db/migration")
}
