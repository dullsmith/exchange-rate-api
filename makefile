
.PHONY: docker-build docker-up docker-start docker-down docker-destroy docker-stop docker-restart docker-logs

docker-build:
	docker-compose -f ./docker/docker-compose.yaml build $(c)
docker-up:
	docker-compose -f ./docker/docker-compose.yaml up -d $(c)
docker-start:
	docker-compose -f ./docker/docker-compose.yaml start $(c)
docker-destroy:
	docker-compose -f ./docker/docker-compose.yaml down -v $(c)
docker-stop:
	docker-compose -f ./docker/docker-compose.yaml stop $(c)
docker-restart:
	docker-compose -f ./docker/docker-compose.yaml stop $(c)
	docker-compose -f ./docker/docker-compose.yaml up -d $(c)
docker-logs:
	docker-compose -f ./docker/docker-compose.yaml logs --tail=100 -f $(c)

pre-commit: setup-pre-commit update-pre-commit

setup-pre-commit:
	pre-commit install

update-pre-commit:
	pre-commit autoupdate
