package io.exchangerate.app.controller.v1.model

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.math.BigDecimal


class ExchangeRateResponseTest {
    @Test
    fun `Validate inverse exchange rate is correct`() {
        // when
        val currencyResponse = ExchangeRateResponse(
            "GBP",
            BigDecimal.valueOf(0.86458)
        )
        // then
        Assertions.assertEquals("GBP", currencyResponse.currency)
        Assertions.assertEquals(BigDecimal.valueOf(0.86458), currencyResponse.rate)
        Assertions.assertEquals(BigDecimal.valueOf(1.15663), currencyResponse.inverseRate)
    }
}
