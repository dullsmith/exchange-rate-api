package io.exchangerate.app.controller.v1

import com.ninjasquad.springmockk.MockkBean
import io.exchangerate.app.controller.v1.model.DatedExchangeRateResponse
import io.exchangerate.app.controller.v1.model.ExchangeRateResponse
import io.exchangerate.app.exceptions.CurrencyNotAvailableException
import io.exchangerate.app.service.ExchangeRateServiceImpl
import io.mockk.every
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.io.IOException
import java.math.BigDecimal.valueOf
import java.time.LocalDate

@WebMvcTest(DailyExchangeRateController::class)
class DailyExchangeRateControllerTest {
    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockkBean
    private lateinit var service: ExchangeRateServiceImpl

    @BeforeEach
    fun setup() {
        every { service.availableCurrencies() } returns setOf("PLN", "EUR")
    }

    @Test
    fun whenCurrenciesEndpointIsCalled_thenResultIsSuccessful() {
        // when
        mockMvc.perform(get("/daily/available-currencies"))
            // then
            .andExpect(status().isOk)
    }

    @Test
    fun whenCurrenciesEndpointIsCalled_thenResultContainsMockResponse() {
        // when
        mockMvc.perform(get("/daily/available-currencies"))
            // then
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.[0]").value("PLN"))
            .andExpect(jsonPath("$.[1]").value("EUR"))
    }

    @Test
    fun whenExchangeRateForCurrencyEndpointIsCalled_thenResultContainsMockResponse() {
        // given
        val currency = "PLN"
        every { service.dailyExchangeRateFor(currency) } returns DatedExchangeRateResponse(
            LocalDate.of(2023, 1, 1),
            listOf(
                ExchangeRateResponse(
                    currency,
                    valueOf(4)
                )
            )

        )
        // when
        mockMvc.get("/daily/exchange-rate/{currency}", currency)
            .andExpect {
                status { isOk() }
                content {
                    json(
                        """{
                            "date": "2023-01-01",
                            "rates": [{
                                "currency": "PLN",
                                "rate": 4,
                                "inverseRate": 0.25000
                            }]}""",
                    )
                }
            }
    }

    @Test
    fun whenExchangeRateForNonExistentCurrencyEndpointIsCalled_thenReturnErrorResponse() {
        // given
        val currency = "HRK"
        every { service.dailyExchangeRateFor(currency) } throws
                CurrencyNotAvailableException("Currency $currency not available in ECB daily exchange rate response")
        // when
        mockMvc.get("/daily/exchange-rate/{currency}", currency) {
        }.andExpect {
            status { is4xxClientError() }
            content {
                json(
                    """{
                    "status": 404,
                    "message": "Currency $currency not available in ECB daily exchange rate response"
                }""",
                )
            }
        }
    }

    @Test
    fun whenDailyExchangeRatesIsCalled_thenResultContainsMockResponse() {
        // given
        val datedExchangeRateResponse = listOf(
            DatedExchangeRateResponse(
                LocalDate.of(2020, 2, 1),
                listOf(
                    ExchangeRateResponse("USD", valueOf(1.0570)),
                    ExchangeRateResponse("JPY", valueOf(143.55)),
                ),
            )
        )
        every { service.ecbDailyExchangeRates() } returns datedExchangeRateResponse
        // when
        mockMvc.get("/daily/ecb-exchange-rates") {
        }.andExpect {
            status { isOk() }
            content {
                json(
                    """[
                  {
                    "date": "2020-02-01",
                    "rates": [
                      {
                        "currency": "USD",
                        "rate": 1.0570,
                        "inverseRate": 0.94607
                      },
                      {
                        "currency": "JPY",
                        "rate": 143.55,
                        "inverseRate": 0.00697
                      }
                    ]
                  }
                ]""",
                )
            }
        }
    }

    @Test
    fun whenDailyExchangeRatesIsCalled_andSomeExceptionIsThrown_thenReturnErrorResponse() {
        // given
        every { service.ecbDailyExchangeRates() } throws IOException("Some exception message")
        // when
        mockMvc.get("/daily/ecb-exchange-rates") {
        }.andExpect {
            status { is5xxServerError() }
            content {
                json(
                    """{
                    "status": 500,
                    "message": "Some exception message"
                }""",
                )
            }
        }
    }
}
