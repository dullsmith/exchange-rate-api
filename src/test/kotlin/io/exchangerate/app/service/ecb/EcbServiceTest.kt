package io.exchangerate.app.service.ecb

import com.ninjasquad.springmockk.MockkBean
import io.exchangerate.app.exceptions.EcbConnectorException
import io.exchangerate.app.service.ecb.dto.CubeDto
import io.exchangerate.app.service.ecb.dto.DailyReferenceRatesDto
import io.exchangerate.app.service.ecb.dto.EnvelopeDto
import io.exchangerate.app.service.ecb.dto.ReferenceRateDto
import io.mockk.every
import io.mockk.mockk
import okhttp3.ResponseBody
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.annotation.DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD
import retrofit2.Response
import java.math.BigDecimal.valueOf
import java.time.LocalDate

@DirtiesContext(classMode = BEFORE_EACH_TEST_METHOD)
@SpringBootTest(classes = [EcbConnector::class, EcbService::class])
class EcbServiceTest {
    @MockkBean
    private lateinit var connector: EcbConnector

    @Autowired
    private lateinit var service: EcbService

    @Test
    fun `Should successfully map last 90 days rates`() {
        // given
        val envelopeDto =
            EnvelopeDto(
                CubeDto(
                    listOf(
                        DailyReferenceRatesDto(
                            LocalDate.of(2023, 1, 1),
                            listOf(
                                ReferenceRateDto("BGN", valueOf(1.9558)),
                                ReferenceRateDto("CZK", valueOf(23.643)),
                                ReferenceRateDto("DKK", valueOf(7.4438)),
                                ReferenceRateDto("GBP", valueOf(0.88245)),
                            ),
                        ),
                        DailyReferenceRatesDto(
                            LocalDate.of(2023, 1, 2),
                            listOf(
                                ReferenceRateDto("BGN", valueOf(1.9658)),
                                ReferenceRateDto("CZK", valueOf(23.765)),
                                ReferenceRateDto("DKK", valueOf(7.4438)),
                                ReferenceRateDto("GBP", valueOf(1.88245)),
                            ),
                        ),
                    ),
                ),
            )
        every { connector.getLast90DaysRates().execute() } returns Response.success(envelopeDto)
        // when
        val sut = service.getLast90DaysExchangeRatesResponse()
        // then
        assertEquals(envelopeDto.cubeDto.exchangeRates.size, sut.cubeDto.exchangeRates.size)
        assertEquals(LocalDate.of(2023, 1, 1), sut.cubeDto.exchangeRates[0].time)
        assertEquals("BGN", sut.cubeDto.exchangeRates[0].rates[0].currency)
        assertEquals("0.88245".toBigDecimal(), sut.cubeDto.exchangeRates[0].rates[3].rate)
        assertEquals(LocalDate.of(2023, 1, 2), sut.cubeDto.exchangeRates[1].time)
        assertEquals("CZK", sut.cubeDto.exchangeRates[1].rates[1].currency)
        assertEquals("7.4438".toBigDecimal(), sut.cubeDto.exchangeRates[1].rates[2].rate)
    }

    @Test
    fun `Should successfully map historical rates`() {
        // given
        val envelopeDto =
            EnvelopeDto(
                CubeDto(
                    listOf(
                        DailyReferenceRatesDto(
                            LocalDate.of(2023, 1, 1),
                            listOf(
                                ReferenceRateDto("BGN", valueOf(1.9558)),
                                ReferenceRateDto("CZK", valueOf(23.643)),
                                ReferenceRateDto("DKK", valueOf(7.4438)),
                                ReferenceRateDto("GBP", valueOf(0.88245)),
                            ),
                        ),
                        DailyReferenceRatesDto(
                            LocalDate.of(2023, 1, 2),
                            listOf(
                                ReferenceRateDto("BGN", valueOf(1.9658)),
                                ReferenceRateDto("CZK", valueOf(23.765)),
                                ReferenceRateDto("DKK", valueOf(7.4438)),
                                ReferenceRateDto("GBP", valueOf(1.88245)),
                            ),
                        ),
                        DailyReferenceRatesDto(
                            LocalDate.of(1999, 1, 10),
                            listOf(
                                ReferenceRateDto("BGN", valueOf(0.9658)),
                                ReferenceRateDto("CZK", valueOf(21.765)),
                            ),
                        ),
                    ),
                ),
            )
        every { connector.getAllHistoricalRates().execute() } returns Response.success(envelopeDto)
        // when
        val sut = service.getHistoricalExchangeRatesResponse()
        // then
        assertEquals(envelopeDto.cubeDto.exchangeRates.size, sut.cubeDto.exchangeRates.size)
        assertEquals(LocalDate.of(2023, 1, 1), sut.cubeDto.exchangeRates[0].time)
        assertEquals("BGN", sut.cubeDto.exchangeRates[0].rates[0].currency)
        assertEquals("0.88245".toBigDecimal(), sut.cubeDto.exchangeRates[0].rates[3].rate)
        assertEquals(LocalDate.of(2023, 1, 2), sut.cubeDto.exchangeRates[1].time)
        assertEquals("CZK", sut.cubeDto.exchangeRates[1].rates[1].currency)
        assertEquals("7.4438".toBigDecimal(), sut.cubeDto.exchangeRates[1].rates[2].rate)
        assertEquals(LocalDate.of(1999, 1, 10), sut.cubeDto.exchangeRates[2].time)
        assertEquals("CZK", sut.cubeDto.exchangeRates[2].rates[1].currency)
        assertEquals("21.765".toBigDecimal(), sut.cubeDto.exchangeRates[2].rates[1].rate)
    }

    @Test
    fun `Should successfully map daily rates`() {
        // given
        val envelopeDto =
            EnvelopeDto(
                CubeDto(
                    listOf(
                        DailyReferenceRatesDto(
                            LocalDate.of(2023, 1, 5),
                            listOf(
                                ReferenceRateDto("TRY", valueOf(20.0628)),
                                ReferenceRateDto("AUD", valueOf(1.5728)),
                            ),
                        ),
                    ),
                ),
            )
        every { connector.getDailyRates().execute() } returns Response.success(envelopeDto)
        // when
        val sut = service.getDailyExchangeRatesResponse()
        // then
        assertEquals(envelopeDto.cubeDto.exchangeRates.size, sut.cubeDto.exchangeRates.size)
        assertEquals(LocalDate.of(2023, 1, 5), sut.cubeDto.exchangeRates[0].time)
        assertEquals("TRY", sut.cubeDto.exchangeRates[0].rates[0].currency)
        assertEquals("1.5728".toBigDecimal(), sut.cubeDto.exchangeRates[0].rates[1].rate)
    }

    @Test
    fun `Should fail when response body is null for daily rates`() {
        // given
        every { connector.getDailyRates().execute() } returns Response.success(null)
        // when
        val result: () -> Unit = { service.getDailyExchangeRatesResponse() }
        // then
        assertThrows<EcbConnectorException>(result)
    }

    @Test
    fun `Should fail when response is not successful for historical rates`() {
        // given
        val mockkResponseBody = mockk<ResponseBody>(relaxed = true)
        every { connector.getAllHistoricalRates().execute() } returns Response.error(404, mockkResponseBody)
        // when
        val result: () -> Unit = { service.getHistoricalExchangeRatesResponse() }
        // then
        assertThrows<EcbConnectorException>(result)
    }

    @Test
    fun `Should fail when response is not successful for last 90 days rates`() {
        assertThrows<EcbConnectorException> {
            // given
            val mockkResponseBody = mockk<ResponseBody>(relaxed = true)
            every { connector.getLast90DaysRates().execute() } returns Response.error(404, mockkResponseBody)
            // when
            service.getLast90DaysExchangeRatesResponse()
        }
    }
}
