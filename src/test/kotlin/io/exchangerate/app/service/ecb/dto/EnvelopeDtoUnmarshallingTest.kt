package io.exchangerate.app.service.ecb.dto

import com.fasterxml.jackson.databind.exc.InvalidFormatException
import com.fasterxml.jackson.databind.exc.ValueInstantiationException
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.io.File
import java.math.BigDecimal.valueOf
import java.time.LocalDate

class EnvelopeDtoUnmarshallingTest {
    private fun unmarshall(filename: String): EnvelopeDto {
        val xmlMapper = XmlMapper()
        return xmlMapper.readValue(File(filename), EnvelopeDto::class.java)
    }

    @Test
    fun `Unmarshalling daily exchange rates to envelope works accurately`() {
        // when
        val result = unmarshall("src/test/resources/eurofxref-daily.xml")
        // then
        assertNotNull(result)
        assertEquals(1, result.cubeDto.exchangeRates.size)
        assertEquals(LocalDate.of(2023, 3, 3), result.cubeDto.exchangeRates[0].time)
        assertEquals(30, result.cubeDto.exchangeRates[0].rates.size)
        // USD index 0
        assertEquals("USD", result.cubeDto.exchangeRates[0].rates[0].currency)
        assertEquals(valueOf(1.0615), result.cubeDto.exchangeRates[0].rates[0].rate)
        // ISK index 12
        assertEquals("ISK", result.cubeDto.exchangeRates[0].rates[11].currency)
        assertEquals("150.30".toBigDecimal(), result.cubeDto.exchangeRates[0].rates[11].rate)
        // ZAR index 29
        assertEquals("ZAR", result.cubeDto.exchangeRates[0].rates[29].currency)
        assertEquals(valueOf(19.2837), result.cubeDto.exchangeRates[0].rates[29].rate)
    }

    @Test
    fun `Unmarshalling historical exchange rates to envelope works accurately`() {
        // when
        val result = unmarshall("src/test/resources/eurofxref-hist-90d.xml")
        // then
        assertNotNull(result)
        assertEquals(64, result.cubeDto.exchangeRates.size)

        assertEquals(LocalDate.of(2023, 3, 3), result.cubeDto.exchangeRates[0].time)
        assertEquals(30, result.cubeDto.exchangeRates[0].rates.size)

        assertEquals("USD", result.cubeDto.exchangeRates[0].rates[0].currency)
        assertEquals(valueOf(1.0615), result.cubeDto.exchangeRates[0].rates[0].rate)

        assertEquals(LocalDate.of(2023, 2, 28), result.cubeDto.exchangeRates[3].time)
        assertEquals(30, result.cubeDto.exchangeRates[3].rates.size)

        assertEquals("CZK", result.cubeDto.exchangeRates[3].rates[3].currency)
        assertEquals(valueOf(23.497), result.cubeDto.exchangeRates[3].rates[3].rate)

        assertEquals(LocalDate.of(2022, 12, 5), result.cubeDto.exchangeRates[63].time)
        assertEquals(31, result.cubeDto.exchangeRates[63].rates.size)

        assertEquals("HRK", result.cubeDto.exchangeRates[63].rates[13].currency)
        assertEquals(valueOf(7.551), result.cubeDto.exchangeRates[63].rates[13].rate)
    }

    @Test
    fun `Unmarshalling broken exchange rate in daily exchange rates to envelope should throw exception`() {
        // when
        val result: () -> Unit = { unmarshall("src/test/resources/eurofxref-daily-broken-rate.xml") }
        // then
        assertThrows<ValueInstantiationException>(result)
    }

    @Test
    fun `Unmarshalling broken date in daily exchange rates to envelope should throw exception`() {
        // when
        val result: () -> Unit = { unmarshall("src/test/resources/eurofxref-daily-broken-date.xml") }
        // then
        assertThrows<InvalidFormatException>(result)
    }

    @Test
    fun `Unmarshalling empty date in daily exchange rates to envelope should throw exception`() {
        // when
        val result: () -> Unit = { unmarshall("src/test/resources/eurofxref-daily-empty-date.xml") }
        // then
        assertThrows<ValueInstantiationException>(result)
    }
}
