package io.exchangerate.app.service

import com.ninjasquad.springmockk.MockkBean
import io.exchangerate.app.controller.v1.model.DatedExchangeRateResponse
import io.exchangerate.app.controller.v1.model.ExchangeRateResponse
import io.exchangerate.app.exceptions.CurrencyNotAvailableException
import io.exchangerate.app.service.ecb.EcbService
import io.exchangerate.app.service.ecb.dto.CubeDto
import io.exchangerate.app.service.ecb.dto.DailyReferenceRatesDto
import io.exchangerate.app.service.ecb.dto.EnvelopeDto
import io.exchangerate.app.service.ecb.dto.ReferenceRateDto
import io.mockk.every
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.time.LocalDate

@SpringBootTest(classes = [EcbService::class, ExchangeRateServiceImpl::class, ExchangeRateMapper::class])
class ExchangeRateServiceImplTest {
    @MockkBean
    private lateinit var ecbService: EcbService

    @Autowired
    private lateinit var exchangeRateService: ExchangeRateService

    private val envelopeDto =
        EnvelopeDto(
            CubeDto(
                listOf(
                    DailyReferenceRatesDto(
                        LocalDate.of(2021, 1, 1),
                        listOf(
                            ReferenceRateDto("BGN", "1.9558".toBigDecimal()),
                            ReferenceRateDto("CZK", "23.643".toBigDecimal()),
                            ReferenceRateDto("DKK", "7.4438".toBigDecimal()),
                            ReferenceRateDto("GBP", "0.88245".toBigDecimal()),
                            ReferenceRateDto("NOK", "11.0610".toBigDecimal()),
                        ),
                    ),
                ),
            ),
        )

    @BeforeEach
    fun setup() {
        every { ecbService.getDailyExchangeRatesResponse() } returns envelopeDto
    }

    @Test
    fun `Available currencies should return BGN, CZK, DKK, GBP and NOK`() {
        // when
        val result = exchangeRateService.availableCurrencies()
        // then
        assertEquals(setOf("BGN", "CZK", "DKK", "GBP", "NOK"), result)
    }

    @TestFactory
    fun `Should return correct exchange rate`() =
        listOf(
            "BGN" to "1.9558".toBigDecimal(),
            "CZK" to "23.643".toBigDecimal(),
            "DKK" to "7.4438".toBigDecimal(),
            "GBP" to "0.88245".toBigDecimal(),
            "NOK" to "11.0610".toBigDecimal(),
        )
            .map { (input, expected) ->
                DynamicTest.dynamicTest("of $expected for currency $input ") {
                    assertEquals(expected, exchangeRateService.dailyExchangeRateFor(input).rates[0].rate)
                }
            }

    @TestFactory
    fun `Should return correct inverse exchange rate`() =
        listOf(
            "BGN" to "0.51130".toBigDecimal(),
            "CZK" to "0.04230".toBigDecimal(),
            "DKK" to "0.13434".toBigDecimal(),
            "GBP" to "1.13321".toBigDecimal(),
            "NOK" to "0.09041".toBigDecimal(),
        )
            .map { (input, expected) ->
                DynamicTest.dynamicTest("of $expected for currency $input ") {
                    assertEquals(expected, exchangeRateService.dailyExchangeRateFor(input).rates[0].inverseRate)
                }
            }

    @Test
    fun `Should return a correct exchange rate currency response`() {
        // given
        val currency = "NOK"
        // when
        val response = exchangeRateService.dailyExchangeRateFor(currency)
        // then
        assertNotNull(response)
        assertEquals(LocalDate.of(2021, 1, 1), response.date)
        assertEquals("NOK", response.rates[0].currency)
        assertEquals("11.0610".toBigDecimal(), response.rates[0].rate)
        assertEquals("0.09041".toBigDecimal(), response.rates[0].inverseRate)
    }

    @Test
    fun `Should return a list of ecb daily exchange rates`() {
        // given
        val expected =
            listOf(
                DatedExchangeRateResponse(
                    LocalDate.of(2021, 1, 1),
                    listOf(
                        ExchangeRateResponse("BGN", "1.9558".toBigDecimal()),
                        ExchangeRateResponse("CZK", "23.643".toBigDecimal()),
                        ExchangeRateResponse("DKK", "7.4438".toBigDecimal()),
                        ExchangeRateResponse("GBP", "0.88245".toBigDecimal()),
                        ExchangeRateResponse("NOK", "11.0610".toBigDecimal()),
                    ),
                )
            )
        // when
        val result = exchangeRateService.ecbDailyExchangeRates()
        // then
        assertEquals(
            expected,
            result,
        )
    }

    @Test
    fun `When currency is not present should throw exception`() {
        // given
        val currency = "BTC"
        every { ecbService.getDailyExchangeRatesResponse() } returns envelopeDto
        // when
        val result: () -> Unit = { exchangeRateService.dailyExchangeRateFor(currency) }
        // then
        assertThrows<CurrencyNotAvailableException>(result)
    }
}
