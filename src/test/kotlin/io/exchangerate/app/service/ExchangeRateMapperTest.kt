package io.exchangerate.app.service

import io.exchangerate.app.controller.v1.model.DatedExchangeRateResponse
import io.exchangerate.app.controller.v1.model.ExchangeRateResponse
import io.exchangerate.app.exceptions.CurrencyNotAvailableException
import io.exchangerate.app.service.ecb.dto.CubeDto
import io.exchangerate.app.service.ecb.dto.DailyReferenceRatesDto
import io.exchangerate.app.service.ecb.dto.EnvelopeDto
import io.exchangerate.app.service.ecb.dto.ReferenceRateDto
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.assertThrows
import java.time.LocalDate

class ExchangeRateMapperTest {

    private var mapper = ExchangeRateMapper()

    private val envelopeDto =
        EnvelopeDto(
            CubeDto(
                listOf(
                    DailyReferenceRatesDto(
                        LocalDate.of(2023, 3, 2),
                        listOf(
                            ReferenceRateDto("BGN", "1.9558".toBigDecimal()),
                            ReferenceRateDto("CZK", "23.643".toBigDecimal()),
                            ReferenceRateDto("DKK", "7.4438".toBigDecimal()),
                            ReferenceRateDto("GBP", "0.88245".toBigDecimal()),
                            ReferenceRateDto("NOK", "11.0610".toBigDecimal()),
                        ),
                    ),
                    DailyReferenceRatesDto(
                        LocalDate.of(2023, 3, 1),
                        listOf(
                            ReferenceRateDto("BGN", "1.9534".toBigDecimal()),
                            ReferenceRateDto("CZK", "23.553".toBigDecimal()),
                            ReferenceRateDto("DKK", "7.4422".toBigDecimal()),
                            ReferenceRateDto("GBP", "1.88245".toBigDecimal()),
                        ),
                    ),
                    DailyReferenceRatesDto(
                        LocalDate.of(2022, 2, 1),
                        listOf(
                            ReferenceRateDto("BGN", "1.9434".toBigDecimal()),
                            ReferenceRateDto("CZK", "22.553".toBigDecimal()),
                            ReferenceRateDto("DKK", "6.4422".toBigDecimal()),
                            ReferenceRateDto("GBP", "1.88445".toBigDecimal()),
                        ),
                    ),
                ),
            ),
        )

    private val datedExchangeRateResponse =
        listOf(
            DatedExchangeRateResponse(
                LocalDate.of(2023, 3, 2),
                listOf(
                    ExchangeRateResponse("BGN", "1.9558".toBigDecimal()),
                    ExchangeRateResponse("CZK", "23.643".toBigDecimal()),
                    ExchangeRateResponse("DKK", "7.4438".toBigDecimal()),
                    ExchangeRateResponse("GBP", "0.88245".toBigDecimal()),
                    ExchangeRateResponse("NOK", "11.0610".toBigDecimal()),
                ),
            ),
            DatedExchangeRateResponse(
                LocalDate.of(2023, 3, 1),
                listOf(
                    ExchangeRateResponse("BGN", "1.9534".toBigDecimal()),
                    ExchangeRateResponse("CZK", "23.553".toBigDecimal()),
                    ExchangeRateResponse("DKK", "7.4422".toBigDecimal()),
                    ExchangeRateResponse("GBP", "1.88245".toBigDecimal()),
                ),
            ),
            DatedExchangeRateResponse(
                LocalDate.of(2022, 2, 1),
                listOf(
                    ExchangeRateResponse("BGN", "1.9434".toBigDecimal()),
                    ExchangeRateResponse("CZK", "22.553".toBigDecimal()),
                    ExchangeRateResponse("DKK", "6.4422".toBigDecimal()),
                    ExchangeRateResponse("GBP", "1.88445".toBigDecimal()),
                ),
            ),
        )

    @Test
    fun `Should successfully return envelope response for exchange rates request`() {
        // when
        val result = mapper.mapExchangeRatesResponse(envelopeDto)
        // then
        Assertions.assertEquals(
            datedExchangeRateResponse,
            result,
        )
    }

    @Test
    fun `Available currencies should return BGN, CZK, DKK, GBP and NOK`() {
        // when
        val result = mapper.mapDailyAvailableCurrencies(envelopeDto)
        // then
        Assertions.assertEquals(setOf("BGN", "CZK", "DKK", "GBP", "NOK"), result)
    }


    @Test
    fun `When non existent currency is requested should throw exception`() {
        // given
        val currency = "HRK"
        // when
        val result: () -> Unit = { mapper.mapDailyExchangeRateByCurrency(envelopeDto, currency) }
        // then
        assertThrows<CurrencyNotAvailableException>(result)
    }

    @TestFactory
    fun `Should return correct exchange rate`() =
        listOf(
            "BGN" to "1.9558".toBigDecimal(),
            "CZK" to "23.643".toBigDecimal(),
            "DKK" to "7.4438".toBigDecimal(),
            "GBP" to "0.88245".toBigDecimal(),
            "NOK" to "11.0610".toBigDecimal(),
        )
            .map { (input, expected) ->
                DynamicTest.dynamicTest("of $expected for currency $input ") {
                    Assertions.assertEquals(
                        expected,
                        mapper.mapDailyExchangeRateByCurrency(envelopeDto, input).rates[0].rate
                    )
                }
            }


    @Test
    fun `Should successfully return daily response for exchange rates by currency`() {
        // given
        val currency = "CZK"
        // when
        val result = mapper.mapDailyExchangeRateByCurrency(envelopeDto, currency)
        // then
        Assertions.assertEquals(
            DatedExchangeRateResponse(
                LocalDate.of(2023, 3, 2),
                listOf(
                    ExchangeRateResponse(
                        "CZK",
                        "23.643".toBigDecimal()
                    )
                )
            ),
            result,
        )
    }

    @Test
    fun `Should successfully return yearly response for exchange rates by currency`() {
        // given
        val currency = setOf("CZK")
        // when
        val result = mapper.mapYearlyExchangeRateByCurrency(envelopeDto, currency, "2023")
        // then
        Assertions.assertEquals(
            listOf(
                DatedExchangeRateResponse(
                    LocalDate.of(2023, 3, 2),
                    listOf(
                        ExchangeRateResponse(
                            "CZK",
                            "23.643".toBigDecimal()
                        )
                    )
                ),
                DatedExchangeRateResponse(
                    LocalDate.of(2023, 3, 1),
                    listOf(
                        ExchangeRateResponse(
                            "CZK",
                            "23.553".toBigDecimal()
                        )
                    )
                ),
            ),
            result,
        )
    }
}
