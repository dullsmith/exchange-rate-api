CREATE TABLE exchange_rate_history
(
    id                          SERIAL UNIQUE PRIMARY KEY,
    exchange_rate_date          TEXT       NOT NULL,
    from_currency               VARCHAR(3) NOT NULL,
    to_currency                 VARCHAR(3) NOT NULL,
    rate                        TEXT       NOT NULL
);

CREATE SEQUENCE IF NOT EXISTS exchange_rate_history_id_seq INCREMENT BY 10;
