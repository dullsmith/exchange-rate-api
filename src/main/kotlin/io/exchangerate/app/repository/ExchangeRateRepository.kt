package io.exchangerate.app.repository

import io.exchangerate.app.repository.model.ExchangeRateHistory
import org.springframework.data.repository.CrudRepository

interface ExchangeRateRepository : CrudRepository<ExchangeRateHistory, String>
