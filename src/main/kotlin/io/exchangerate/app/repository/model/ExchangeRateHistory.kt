package io.exchangerate.app.repository.model

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.Table

//https://www.jpa-buddy.com/blog/best-practices-and-common-pitfalls/

@Entity
@Table(name = "exchange_rate_history")
open class ExchangeRateHistory(

    @Column(name = "exchange_rate_date", nullable = false)
    open var exchangeRateDate: String? = null,

    @Column(name = "from_currency", nullable = false)
    open var fromCurrency: String? = null,

    @Column(name = "to_currency", nullable = false)
    open var toCurrency: String? = null,

    @Column(name = "rate", nullable = false)
    open var rate: String? = null,

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    open var id: String? = null
)
