package io.exchangerate.app.service

import io.exchangerate.app.controller.v1.model.DatedExchangeRateResponse
import io.exchangerate.app.controller.v1.model.ExchangeRateResponse
import io.exchangerate.app.exceptions.CurrencyNotAvailableException
import io.exchangerate.app.service.ecb.dto.EnvelopeDto
import org.springframework.stereotype.Component

@Component
class ExchangeRateMapper {

    fun mapDailyAvailableCurrencies(envelopeDto: EnvelopeDto): Set<String> {
        return envelopeDto.cubeDto.exchangeRates.first().rates.map { it.currency }.toSet()
    }

    fun mapExchangeRatesResponse(envelopeDto: EnvelopeDto): List<DatedExchangeRateResponse> {
        val listOfExchangeRates = mutableListOf<DatedExchangeRateResponse>()

        envelopeDto.cubeDto.exchangeRates.forEach {
            val exchangeRates = it.rates
                .map { xr ->
                    ExchangeRateResponse(xr.currency, xr.rate)
                }
                .toList()
            listOfExchangeRates.add(DatedExchangeRateResponse(it.time, exchangeRates))
        }

        return listOfExchangeRates
    }

    fun mapDailyExchangeRateByCurrency(envelopeDto: EnvelopeDto, currencies: String): DatedExchangeRateResponse {
        return mapExchangeRateByCurrency(envelopeDto, setOf(currencies)).first()
    }

    fun mapYearlyExchangeRateByCurrency(
        envelopeDto: EnvelopeDto,
        currencies: Set<String>,
        year: String
    ): List<DatedExchangeRateResponse> {
        return mapExchangeRateByCurrency(envelopeDto, currencies)
            .filter { it.date.year == year.toInt() }
    }

    private fun mapExchangeRateByCurrency(
        envelopeDto: EnvelopeDto,
        currencies: Set<String>
    ): List<DatedExchangeRateResponse> {

        val response = mutableListOf<DatedExchangeRateResponse>()

        mapExchangeRatesResponse(envelopeDto)
            .map {
                response.add(DatedExchangeRateResponse(
                    it.date,
                    it.rates.filter { c -> currencies.contains(c.currency) }
                ))
            }

        return if (response.first().rates.isEmpty()) {
            throw CurrencyNotAvailableException("Currencies $currencies not present in ECB response")
        } else {
            response
        }
    }
}
