package io.exchangerate.app.service

import io.exchangerate.app.controller.v1.model.DatedExchangeRateResponse
import io.exchangerate.app.exceptions.PageNotFoundException
import io.exchangerate.app.exceptions.YearOutOfLimitsException
import io.exchangerate.app.service.ecb.EcbService
import mu.KotlinLogging
import org.springframework.stereotype.Service
import java.time.Year

@Service
class HistoricalExchangeRateServiceImpl(
    val ecbService: EcbService,
    val exchangeRateMapper: ExchangeRateMapper
) : HistoricalExchangeRateService {

    private val log = KotlinLogging.logger {}

    override fun historicalExchangeRates(): List<DatedExchangeRateResponse> {
        val response = ecbService.getHistoricalExchangeRatesResponse()
        log.info { "Received response for ECB historical exchange rates" }
        return exchangeRateMapper.mapExchangeRatesResponse(response)
    }

    override fun pagedHistoricalExchangeRates(page: Int, size: Int): List<DatedExchangeRateResponse> {
        val response = ecbService.getHistoricalExchangeRatesResponse()
        log.info { "Received response for ECB historical exchange rates" }

        val rates = exchangeRateMapper.mapExchangeRatesResponse(response).chunked(size)

        if (page - 1 > rates.size) {
            throw PageNotFoundException("Page $page not found, last page is ${rates.size}")
        }

        return rates[page - 1]
    }

    override fun yearlyExchangeRates(year: String, currencies: Set<String>): List<DatedExchangeRateResponse> {

        if (year.toInt() > Year.now().value) {
            throw YearOutOfLimitsException("Requested year $year is in the future")
        }

        val response = ecbService.getHistoricalExchangeRatesResponse()
        log.info { "Received response for ECB historical exchange rates" }
        return exchangeRateMapper.mapYearlyExchangeRateByCurrency(response, currencies, year)
    }

    override fun last90DaysExchangeRates(): List<DatedExchangeRateResponse> {
        val response = ecbService.getLast90DaysExchangeRatesResponse()
        log.info { "Received response for ECB historical exchange rates" }
        return exchangeRateMapper.mapExchangeRatesResponse(response)
    }

}
