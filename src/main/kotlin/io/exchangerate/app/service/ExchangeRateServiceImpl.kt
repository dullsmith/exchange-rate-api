package io.exchangerate.app.service

import io.exchangerate.app.controller.v1.model.DatedExchangeRateResponse
import io.exchangerate.app.service.ecb.EcbService
import mu.KotlinLogging
import org.springframework.stereotype.Service

@Service
class ExchangeRateServiceImpl(
    val ecbService: EcbService,
    val exchangeRateMapper: ExchangeRateMapper
) : ExchangeRateService {

    private val log = KotlinLogging.logger {}

    override fun availableCurrencies(): Set<String> {
        val response = ecbService.getDailyExchangeRatesResponse()
        log.info { "Handling response for ECB daily exchange rates" }
        return exchangeRateMapper.mapDailyAvailableCurrencies(response)
    }

    override fun dailyExchangeRateFor(currency: String): DatedExchangeRateResponse {
        val response = ecbService.getDailyExchangeRatesResponse()
        log.info { "Handling response for ECB daily exchange rates" }
        return exchangeRateMapper.mapDailyExchangeRateByCurrency(response, currency)
    }

    override fun ecbDailyExchangeRates(): List<DatedExchangeRateResponse> {
        val response = ecbService.getDailyExchangeRatesResponse()
        log.info { "Handling response for ECB daily exchange rates" }
        return exchangeRateMapper.mapExchangeRatesResponse(response)
    }
}
