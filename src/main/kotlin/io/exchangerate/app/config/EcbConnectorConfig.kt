package io.exchangerate.app.config

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinFeature
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.exchangerate.app.service.ecb.EcbConnector
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory

@Configuration
class EcbConnectorConfig {

    @Value("\${ecb.url}")
    private lateinit var ecbUrl: String

    @Bean
    fun ecbConnector(): EcbConnector {
        return Retrofit.Builder()
            .baseUrl(ecbUrl)
            .addConverterFactory(JacksonConverterFactory.create(xmlMapperFactory()))
            .build()
            .create(EcbConnector::class.java)
    }


    private fun xmlMapperFactory(): XmlMapper {
        return XmlMapper().apply {
            registerModule(JacksonXmlModule())
            registerModule(
                KotlinModule.Builder()
                    .configure(KotlinFeature.NullToEmptyCollection, false)
                    .configure(KotlinFeature.NullToEmptyMap, false)
                    .configure(KotlinFeature.NullIsSameAsDefault, false)
                    .configure(KotlinFeature.SingletonSupport, false)
                    .configure(KotlinFeature.StrictNullChecks, false)
                    .build()
            )
            registerModule(
                JavaTimeModule()
            )
            configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            enable(SerializationFeature.INDENT_OUTPUT)
        }
    }
}
