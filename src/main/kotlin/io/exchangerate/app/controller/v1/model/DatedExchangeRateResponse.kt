package io.exchangerate.app.controller.v1.model

import java.math.BigDecimal
import java.math.RoundingMode
import java.time.LocalDate

data class DatedExchangeRateResponse(
    val date: LocalDate,
    val rates: List<ExchangeRateResponse>,
)

data class ExchangeRateResponse(
    val currency: String,
    val rate: BigDecimal,
) {
    val inverseRate: BigDecimal = BigDecimal.ONE.divide(this.rate, 5, RoundingMode.HALF_EVEN)
}
