# Changelog

## [2.0.6](https://github.com/simao-ferreira/exchange-rate-api/compare/v2.0.5...v2.0.6) (2023-10-02)


### Changed

* **infra:** Add note for test workflow ([80925a3](https://github.com/simao-ferreira/exchange-rate-api/commit/80925a3006acf8ab2da213be25eda8616c8250da))
* **main:** Apply ktlint refactoring to tests ([017e93f](https://github.com/simao-ferreira/exchange-rate-api/commit/017e93f80b71102cf3a94c2a8875a216424ce8ee))
* **main:** Refactor tests to make them faster and lighter ([6abb5c9](https://github.com/simao-ferreira/exchange-rate-api/commit/6abb5c9f0ac3091637b1b61259fc7405fef7870d))

## [2.0.5](https://github.com/simao-ferreira/exchange-rate-api/compare/v2.0.4...v2.0.5) (2023-10-02)


### Changed

* **infra:** Workflow for tests adjustment ([0fffd96](https://github.com/simao-ferreira/exchange-rate-api/commit/0fffd96fec49baf16fbe5d7e604899e0b29a3ad5))

## [2.0.4](https://github.com/simao-ferreira/exchange-rate-api/compare/v2.0.3...v2.0.4) (2023-10-02)


### Changed

* **infra:** Another attempt to fix the release workflow ([a78c0b3](https://github.com/simao-ferreira/exchange-rate-api/commit/a78c0b35902998408ef3090a0e6a9d1b7f568bdb))

## [2.0.3](https://github.com/simao-ferreira/exchange-rate-api/compare/v2.0.2...v2.0.3) (2023-10-02)


### Changed

* **infra:** Fix the tweak workflow for release ([ec1268b](https://github.com/simao-ferreira/exchange-rate-api/commit/ec1268b37e3004142f64d999e118caddf588a2dc))

## [2.0.2](https://github.com/simao-ferreira/exchange-rate-api/compare/v2.0.1...v2.0.2) (2023-10-02)


### Changed

* **infra:** Tweak workflows for release ([54c4652](https://github.com/simao-ferreira/exchange-rate-api/commit/54c465201880fac58b47a570eaf16e963c5b9653))

## [2.0.1](https://github.com/simao-ferreira/exchange-rate-api/compare/v2.0.0...v2.0.1) (2023-10-01)


### Changed

* **infra:** Fix release please config to run tests at merge ([c02e07a](https://github.com/simao-ferreira/exchange-rate-api/commit/c02e07ad3a4486729fddc2db2f7cb07d13c8a993))
* **main:** Refactor currency response to include inverse exchange rate ([3b60926](https://github.com/simao-ferreira/exchange-rate-api/commit/3b609266c1e2f944e7d25ae0c897be090260b679))
* **main:** Refactor exchange rate to big decimal ([9da6722](https://github.com/simao-ferreira/exchange-rate-api/commit/9da6722bf9cc3cc62da2431b78307faadaf47fbf))

## [2.0.0](https://github.com/simao-ferreira/exchange-rate-api/compare/v1.0.0...v2.0.0) (2023-10-01)


### ⚠ BREAKING CHANGES

* Include release please github workflow

### Added

* update readme ([7e7ae58](https://github.com/simao-ferreira/exchange-rate-api/commit/7e7ae58cd8e5aacab194ff41edcdf74ea8a39005))


### Changed

* Include release please github workflow ([9742fb4](https://github.com/simao-ferreira/exchange-rate-api/commit/9742fb467c61279ea7ae9a231b57b2060b856caa))
* **infra:** Add git workflow for automated tests ([c63f4ce](https://github.com/simao-ferreira/exchange-rate-api/commit/c63f4ce5862219aec7ec4522a98bbd29aae02836))
* **infra:** Add pre commit checks and apply suggestions ([4e0a6ae](https://github.com/simao-ferreira/exchange-rate-api/commit/4e0a6ae10d61cbb4a48821d24ccea7e599e9dc60))
* **infra:** Change release please type ([d594015](https://github.com/simao-ferreira/exchange-rate-api/commit/d594015fc11173d9e21323439a7d8838968e304b))
* **infra:** Fix release please config ([453f789](https://github.com/simao-ferreira/exchange-rate-api/commit/453f789ec928c20d5292da43a020f28fd8209aab))
* **infra:** Reduce testing workflow only for PR cration ([6ecae1c](https://github.com/simao-ferreira/exchange-rate-api/commit/6ecae1ccbc0cae94ea5352aaa46816fa5da1feb8))
* **infra:** Update gradle version and plugins ([d9af15e](https://github.com/simao-ferreira/exchange-rate-api/commit/d9af15e79cf4509099ad3305e121242e08d80370))
* **main:** release 1.0.0 ([66ad6bb](https://github.com/simao-ferreira/exchange-rate-api/commit/66ad6bba5bbebd72a548e1e713d4f6faeb25bf8d))
* **main:** release 1.0.1-SNAPSHOT ([8e08acc](https://github.com/simao-ferreira/exchange-rate-api/commit/8e08acc1f0ad8332943e2e85656b12a202f21b29))
* **test:** Remove unnecessary dirties context from tests ([8e2f641](https://github.com/simao-ferreira/exchange-rate-api/commit/8e2f6415a9a45aa660b0d771f9d492fe84110dc6))
